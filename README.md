# README #

To determine the convex hull we perform a Graham Scan. This code provides a demo and example of how to code this algorithm.


### How do I get set up? ###

* Pull the repo or go to [Hofware - Convex Hull](http://www.hofware.nl/projects/convex-hull/#a2) and download the GMZ.
* Download GameMaker: Studio and open the project.