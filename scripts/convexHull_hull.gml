///convexHull_hull()

// new list
ds_list_destroy(hullPoints);
hullPoints = ds_list_create();

// add point i=0
ds_list_add(hullPoints, 0);

// iterate :: who is on a counter-clockwise hull
var ccw = 0; var hpLength = 1;

for (var i=1; i < pointCount; i++)
{
    // investigate point 'i', from last confirmed hullPoint
    ccw = convexHull_ccw(hullPoints[| hpLength-1], i, i+1);
    
    // if the step is convex; add point i to hull 
    if (ccw==1) { ds_list_add(hullPoints, i); hpLength++; }
    
    // if concave; traceback 
    if (ccw==-1)
    {
        while (ccw < 0)
        {
            // can't trace further back
            if(hpLength < 1) break;
            
            // check if removing a point makes convex again
            ccw = convexHull_ccw(hullPoints[| hpLength-2], hullPoints[| hpLength-1], i+1);
            if (ccw==-1) { ds_list_delete(hullPoints, hpLength-1); hpLength--; }

        }
    }
}

//show_debug_message("hull size "+string(ds_list_size(hullPoints)));