///convexHull_addPoint(x,y)


var xx = argument0;
var yy = argument1;

/* is called by convexHull to add a point
---------------------------------------*/

ds_list_add(pointX, xx);
ds_list_add(pointY, yy);
ds_list_add(polarAngle, -1);
pointCount++;

return pointCount;