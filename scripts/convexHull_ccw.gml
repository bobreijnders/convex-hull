///convexHull_ccw( a_point, b_point, c_point )

var a = argument0;
var b = argument1;
var c = argument2;

/* counter-clockwise turn
-----------------------------------
returns wether moving from point 'b' to 'c'
forms a counter-clockwise turn.
This requires knowing point 'a' as well.

returns -1 'clockwise'
returns  1 'counter-clockwise'
returns  0 'straight'
*/

// out of bounds
if (b <= a) show_error("convexHull_ccw :: reversed order, from <= to",true);
if (a < 0) show_error("convexHull_ccw :: point index outOfBounds, a < 0 -> "+string(a),true);
if (b < 1) show_error("convexHull_ccw :: point index outOfBounds, b < 1 -> "+string(b),true);
if (b >= pointCount) show_error("convexHull_ccw :: point index outOfBounds, b >= pointCount -> "+string(b),true);

// Circle Definitions //
// point -1 == last point
if (a == -1) a = pointCount-1;

// pointCount = first point
if (c == pointCount) c = 0;


// Cross Product //
var ax = pointX[|a]; var ay = pointY[|a];
var bx = pointX[|b]; var by = pointY[|b];
var cx = pointX[|c]; var cy = pointY[|c];

var crossProduct = (bx-ax)*(cy-ay) - (by-ay)*(cx-ax); 

// Result
if (crossProduct > 0) return -1; //clockwise
if (crossProduct < 0) return  1; //counter-clockwise
return 0; //straight line