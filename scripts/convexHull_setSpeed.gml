///convexHull_setSpeed( point, Vx, Vy)

var idx = argument0;
var Vx = argument1;
var Vy = argument2;

/*
--------------*/

speedX[| idx] = Vx;
speedY[| idx] = Vy;