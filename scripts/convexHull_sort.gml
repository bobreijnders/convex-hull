///convexHull_sort()

/* Based on Insertion Sort
-------------------------------------
Insertion Sort is best for small pointCount or partially ordered

worst case: N^2 / 2
average case: N^2 / 4
best case: N

N(100) =~   12ms
N(1000) =~ 500ms

convexHull will be updating the positions from time to time, 
taking advantage of the previous ordering.
This allows this sort to reach it's near best case performance (N)
*/

// point p0 with smallest y
var p0 = 0; 
var maxY = 0;
for (var i=0; i<pointCount; i++) 
    if (pointY[|i] > maxY) {p0=i; maxY=pointY[|i];}

// polar angles
for (var i=0; i<pointCount; i++) 
{
    polarAngle[|i] = point_direction( 
                        pointX[|p0], pointY[|p0],
                        pointX[|i], pointY[|i]
                        );
}   

//-- Sort on increasing polar angle --//
// starting point
convexHull_sortSwap(p0,0);
// by definition
polarAngle[| 0] = 0;


//show_debug_message("convexHull_sortPoints :: Start sort");
//var timeStart = current_time;

// start insertion sort iteration
for (var i=1; i<pointCount; i++) 
{
    //if(i%100 == 0) show_debug_message("convexHull_sortPoints :: "+string(i));
    for (var j = i; j > 0; j--)
    {
        // Moving from right to left, exchange i with each larger entry to its left
        if (polarAngle[| j] < polarAngle[| j-1]) 
            convexHull_sortSwap(j, j-1);
        else
            break;
    }    
}

//show_debug_message("convexHull_sortPoints :: Sorted "+string(pointCount)+" pnts in "+string(current_time-timeStart)+" ms")