///convexHull_sortSwap( pointA, pointB)

var pA = argument0;
var pB = argument1;


// temp
var tempX = pointX[|pB];
var tempY = pointY[|pB];
var tempVx = speedX[|pB];
var tempVy = speedY[|pB];
var tempAngle = polarAngle[|pB];

// swap A to B
pointX[| pB] = pointX[| pA];
pointY[| pB] = pointY[| pA];
speedX[| pB] = speedX[| pA];
speedY[| pB] = speedY[| pA];
polarAngle[| pB] = polarAngle[| pA];

// swap temp to A
pointX[| pA] = tempX;
pointY[| pA] = tempY;
speedX[| pA] = tempVx;
speedY[| pA] = tempVy;
polarAngle[| pA] = tempAngle;

//done
return true;